﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace ContactApp.Models.Entities
{
    public class Contact
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [NotNull]
        public string Name { get; set; }
        public string Surname { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<PhoneNumber> PhoneNumbers { get; set; } = new List<PhoneNumber>();

        public string Title
        {
            get
            {
                string title = Name;
                if (!string.IsNullOrEmpty(Surname))
                    title += $" {Surname}";

                return title;
            }
        }
    }
}