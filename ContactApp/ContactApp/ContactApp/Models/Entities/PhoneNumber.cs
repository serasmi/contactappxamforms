﻿using System.ComponentModel;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace ContactApp.Models.Entities
{
    public class PhoneNumber
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }
        [ForeignKey(typeof(Contact))]
        public int ContactId { get; set; }
        [NotNull]
        public PhoneNumberType Type { get; set; }
        [MaxLength(11)]
        [NotNull]
        public string Number { get; set; }
        [Ignore]
        public bool IsCorrect { get; set; } = true;
        [Ignore]
        public bool NewNumber { get; set; } = false;
    }
}