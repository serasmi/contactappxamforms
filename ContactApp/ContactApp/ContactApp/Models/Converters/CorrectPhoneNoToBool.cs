﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace ContactApp.Models.Converters
{
    public class CorrectPhoneNoToBool : IValueConverter
    {
        string phoneNumberRegex = @"^(\d{9}|\d{12})$";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                string phoneNumber = (string)value;

                return !Regex.IsMatch(phoneNumber, phoneNumberRegex);
            }
            else
            {
                return true;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
