﻿using System;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace ContactApp.Models.Behaviors
{
    public class PhoneNumberValidatorBehavior : Behavior<Entry>
    {
        string phoneNumberRegex = @"^(\d{9}|\d{12})$";

        public static BindableProperty IsCorrectProperty = BindableProperty.CreateAttached("IsCorrect", typeof(bool), typeof(bool), false, BindingMode.TwoWay);

        public bool IsCorrect
        {
            get { return (bool)GetValue(IsCorrectProperty); }
            set { SetValue(IsCorrectProperty, value); }
        }

        protected override void OnAttachedTo(Entry bindable)
        {
            base.OnAttachedTo(bindable);
            bindable.TextChanged += BindableOnTextChanged;
            bindable.BindingContextChanged += BindableOnBindingContextChanged;
        }

        private void BindableOnBindingContextChanged(object sender, EventArgs e)
        {
            var entry = sender as Entry;
            BindingContext = entry?.BindingContext;
        }

        private void BindableOnTextChanged(object sender, TextChangedEventArgs e)
        {
            var entry = sender as Entry;
            if (!string.IsNullOrEmpty(entry.Text) && Regex.IsMatch(entry.Text.Trim(), phoneNumberRegex))
            {
                IsCorrect = true;
                entry.BackgroundColor = Color.Default;
            }
            else
            {
                IsCorrect = false;
                entry.BackgroundColor = Color.Red;
            }
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            base.OnDetachingFrom(bindable);
            bindable.TextChanged -= BindableOnTextChanged;
            bindable.BindingContextChanged -= BindableOnBindingContextChanged;
        }
    }
}
