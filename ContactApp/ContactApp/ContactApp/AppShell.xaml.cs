﻿using CommonServiceLocator;
using ContactApp.Views;
using Unity;
using Xamarin.Forms;

namespace ContactApp
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute("editcontact", typeof(EditContactPage));
            Routing.RegisterRoute("contactdetails", typeof(ContactDetailsPage));
        }
    }
}
