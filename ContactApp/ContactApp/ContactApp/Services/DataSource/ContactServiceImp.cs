﻿using ContactApp.Models.Entities;
using ContactApp.Models.Extensions;
using SQLite;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactApp.Services.DataSource
{
    public class ContactServiceImp : IContactsService
    {
        static readonly Lazy<SQLiteConnection> lazyInitializer = new Lazy<SQLiteConnection>(() =>
        {
            return new SQLiteConnection(Constants.DatabasePath, Constants.Flags);
        });

        static SQLiteConnection Database => lazyInitializer.Value;
        static bool initialized = false;

        public ContactServiceImp()
        {
            Initialize().SafeFireAndForget(false);
        }

        async Task Initialize()
        {
            if (!initialized)
            {
                if(!Database.TableMappings.Any(m => m.MappedType.Name == typeof(Contact).Name))
                {
                    Database.CreateTable(
                        typeof(Contact));
                    Database.CreateTable(typeof(PhoneNumber));
                    initialized = true;
                }
            }
        }

        public async Task SaveContact(Contact contact)
        {
            if (contact.Id != 0)
            {
                Database.UpdateWithChildren(contact);
            }
            else
            {
                Database.InsertWithChildren(contact);
            }
        }

        public Task<List<Contact>> GetContacts()
        {
            return Task.FromResult(Database.Table<Contact>().ToList());
        }

        public Task DeleteContact(Contact contact)
        {
            return Task.FromResult(Database.Delete(contact));
        }

        public async Task<Contact> GetContact(int id)
        {
            var test = Database.GetWithChildren<Contact>(id);

            return test;
        }

        public Task<List<Contact>> GetContacts(string searchText)
        {
            return Task.FromResult(Database.Table<Contact>().Where(c => c.Name.Contains(searchText) || c.Surname.Contains(searchText)).ToList());
        }
    }
}
