﻿using ContactApp.Models.Entities;
using ContactApp.Models.Extensions;
using SQLite;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ContactApp.Services.DataSource
{
    public class NumbersServiceImp : INumbersService
    {
        static readonly Lazy<SQLiteConnection> lazyInitializer = new Lazy<SQLiteConnection>(() =>
        {
            return new SQLiteConnection(Constants.DatabasePath, Constants.Flags);
        });

        static SQLiteConnection Database => lazyInitializer.Value;

        public Task AddPhoneNumber(PhoneNumber number)
        {
            return Task.FromResult(Database.Insert(number));
        }

        public Task DeletePhoneNumber(PhoneNumber number)
        {
            return Task.FromResult(Database.Delete(number));
        }
    }
}
