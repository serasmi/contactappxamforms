﻿using ContactApp.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ContactApp.Services
{
    public interface IContactsService
    {
        Task<Contact> GetContact(int id);
        Task<List<Contact>> GetContacts();
        Task<List<Contact>> GetContacts(string searchText);
        Task SaveContact(Contact contact);
        Task DeleteContact(Contact contact);
    }
}
