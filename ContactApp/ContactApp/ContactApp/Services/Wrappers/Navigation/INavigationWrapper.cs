﻿using System.Threading.Tasks;

namespace ContactApp.Services.Wrappers.Navigation
{
    public interface INavigationWrapper
    {
        Task GoTo(string uri);
        Task GoToRoot();
    }
}
