﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace ContactApp.Services.Wrappers.Navigation
{
    public class NavigationWrapperImp : INavigationWrapper
    {
        public async Task GoToRoot()
        {
            await Shell.Current.Navigation.PopToRootAsync();
        }

        public async Task GoTo(string uri)
        {
            await Shell.Current.GoToAsync(uri);
        }
    }
}
