﻿using ContactApp.Models.Entities;
using System.Threading.Tasks;

namespace ContactApp.Services
{
    public interface INumbersService
    {
        Task AddPhoneNumber(PhoneNumber number);
        Task DeletePhoneNumber(PhoneNumber number);
    }
}
