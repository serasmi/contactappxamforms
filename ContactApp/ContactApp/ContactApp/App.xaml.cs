﻿using CommonServiceLocator;
using ContactApp.Services;
using ContactApp.Services.DataSource;
using ContactApp.Services.Wrappers.Navigation;
using Unity;
using Unity.ServiceLocation;
using Xamarin.Forms;

namespace ContactApp
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            UnityContainer unityContainer = new UnityContainer();
            unityContainer.RegisterType<IContactsService, ContactServiceImp>();
            unityContainer.RegisterType<INumbersService, NumbersServiceImp>();

            unityContainer.RegisterSingleton<INavigationWrapper, NavigationWrapperImp>();

            ServiceLocator.SetLocatorProvider(() => new UnityServiceLocator(unityContainer));

            MainPage = new AppShell();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
