﻿using CommonServiceLocator;
using ContactApp.Services;
using ContactApp.Services.Wrappers.Navigation;
using ContactApp.ViewModels;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ContactApp.Views
{
    [QueryProperty("ContactIndex", "index")]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditContactPage : ContentPage
    {
        public string ContactIndex
        {
            set
            {
                MyViewModel.ContactId = Uri.UnescapeDataString(value);
            }
        }

        private EditContactViewModel _myViewModel;
        public EditContactViewModel MyViewModel
        {
            get => _myViewModel;
            set => BindingContext = _myViewModel = value;
        }

        public EditContactPage()
        {
            InitializeComponent();
            MyViewModel = new EditContactViewModel(
                ServiceLocator.Current.GetInstance<IContactsService>(),
                ServiceLocator.Current.GetInstance<INumbersService>(),
                ServiceLocator.Current.GetInstance<INavigationWrapper>());
        }
    }
}