﻿using CommonServiceLocator;
using ContactApp.Services;
using ContactApp.Services.Wrappers.Navigation;
using ContactApp.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ContactApp.Views
{
    [QueryProperty("ContactIndex", "index")]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactDetailsPage : ContentPage
    {
        public string ContactIndex
        {
            set
            {
                MyViewModel.ContactId = Uri.UnescapeDataString(value);
            }
        }

        private ContactDetailsViewModel _myViewModel;
        public ContactDetailsViewModel MyViewModel
        {
            get => _myViewModel;
            set => BindingContext = _myViewModel = value;
        }

        public ContactDetailsPage()
        {
            InitializeComponent();

            MyViewModel = new ContactDetailsViewModel(
                ServiceLocator.Current.GetInstance<IContactsService>(),
                ServiceLocator.Current.GetInstance<INumbersService>(),
                ServiceLocator.Current.GetInstance<INavigationWrapper>());

            MessagingCenter.Subscribe<ContactDetailsViewModel, string>(this, "CallButton", (sender, arg) =>
            {
                DisplayAlert("Call button clicked", arg, "OK");
            });

            MessagingCenter.Subscribe<ContactDetailsViewModel, string>(this, "SendMessage", (sender, arg) =>
            {
                DisplayAlert("Send message clicked", arg, "OK");
            });
        }
    }
}