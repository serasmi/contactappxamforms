﻿using CommonServiceLocator;
using ContactApp.Services;
using ContactApp.Services.Wrappers.Navigation;
using ContactApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ContactApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContactsPage : ContentPage
    {
        private ContactsViewModel _myViewModel;
        public ContactsViewModel MyViewModel
        {
            get => _myViewModel;
            set => BindingContext = _myViewModel = value;
        }

        public ContactsPage()
        {
            InitializeComponent();
            MyViewModel = new ContactsViewModel(
                ServiceLocator.Current.GetInstance<IContactsService>(),
                ServiceLocator.Current.GetInstance<INavigationWrapper>());
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (!string.IsNullOrEmpty(SearchEntry.Text))
                SearchEntry.Text = string.Empty;
            MyViewModel.SearchCommand.Execute(null);
        }
    }
}