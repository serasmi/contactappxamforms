﻿using ContactApp.Models.Entities;
using ContactApp.Services;
using ContactApp.Services.Wrappers.Navigation;
using ContactApp.ViewModels.Base;
using ContactApp.ViewModels.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ContactApp.ViewModels
{
    public class ContactsViewModel : Base.BaseViewModel
    {
        public readonly IContactsService ContactsService;
        public readonly INavigationWrapper NavigationWrapper;

        public ICommand SearchCommand { get; set; }
        public ICommand AddItemCommand { get; set; }
        public ICommand SelectionChanged { get; set; }

        private ObservableCollection<Contact> _contacts;
        public ObservableCollection<Contact> Contacts
        {
            get => _contacts;
            set
            {
                _contacts = value;
                OnCollectionChanged(NotifyCollectionChangedAction.Add);
            }
        }

        CancellationTokenSource tokenSource = new CancellationTokenSource();
        CancellationToken token;
        Task searchTask;

        public ContactsViewModel(IContactsService _contactsService, INavigationWrapper _navigationWrapper)
        {
            ContactsService = _contactsService;
            NavigationWrapper = _navigationWrapper;
            token = tokenSource.Token;

            Contacts = new ObservableCollection<Contact>();

            SearchCommand = new RelayCommand(async (object arg) =>
            {
                if (searchTask != null && searchTask.Status == TaskStatus.Running) 
                {
                    tokenSource.Cancel();
                }
                
                await ExecuteSearchCommand(arg);
            });
            AddItemCommand = new RelayCommand(async (object arg) => await ExecuteAddItemCommand());
            SelectionChanged = new RelayCommand(async (object arg) => await OnSelectionChanged(arg));
        }

        public async Task ExecuteAddItemCommand()
        {
            string uri = $"editcontact";
            await NavigationWrapper.GoTo(uri);
        }

        public async Task ExecuteSearchCommand(object arg)
        {
            searchTask = new TaskFactory().StartNew(async (object searchText) =>
            {
                if (string.IsNullOrEmpty((string)searchText))
                {
                    return await ContactsService.GetContacts();
                }
                else
                {
                    return await ContactsService.GetContacts(searchText.ToString());
                }
            }, arg, token).ContinueWith((contacts) => 
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Contacts.Clear();
                    foreach (var contact in contacts.Result.Result)
                        Contacts.Add(contact);
                });
            });
            tokenSource.Dispose();
        }

        public async Task OnSelectionChanged(object arg)
        {
            Contact contact = (Contact)arg;

            string url = $"contactdetails?index={contact.Id}";
            await NavigationWrapper.GoTo(url);
        }
    }
}
