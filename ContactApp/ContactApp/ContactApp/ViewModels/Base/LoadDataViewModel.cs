﻿using ContactApp.Models.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ContactApp.ViewModels.Base
{
    public class LoadDataViewModel : BaseViewModel
    {
        public readonly Func<string, int, int, Task<List<Contact>>> LoadItems;

        private string _search;
        public string Search
        {
            get => _search;
            set
            {
                _search = value;
                OnPropertyChanged();
            }
        }

        private int _page;
        public int Page
        {
            get => _page;
            set => _page = value;
        }

        private bool _isFinalPage = false;
        public bool IsFinalPage
        {
            get => _isFinalPage;
            set => _isFinalPage = value;
        }

        private int ItemsPerPage = 16;

        public LoadDataViewModel(Func<string, int, int, Task<List<Contact>>> _loadItems)
        {
            LoadItems = _loadItems;
        }

        public async Task<List<Contact>> GetItems(bool reload = false)
        {
            IsBusy = true;
            if (reload)
                Page = 0;

            var contacts = new List<Contact>();
            try
            {
                contacts = await LoadItems(Search, Page, ItemsPerPage);
            }
            catch(Exception e)
            {
                throw e;
            }

            Page++;
            IsBusy = false;

            IsFinalPage = contacts.Count % ItemsPerPage == 0 ? false : true;
            return contacts;
        }
    }
}
