﻿using ContactApp.Models.Entities;
using ContactApp.Services;
using ContactApp.Services.Wrappers.Navigation;
using ContactApp.ViewModels.Base;
using ContactApp.ViewModels.Commands;
using ContactApp.Views;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ContactApp.ViewModels
{
    public class ContactDetailsViewModel : BaseViewModel
    {
        public readonly IContactsService ContactsService;
        public readonly INumbersService NumbersService;
        public readonly INavigationWrapper NavigationWrapper;

        public ICommand EditContactCommand { get; set; }
        public ICommand RemoveContactCommand { get; set; }

        public ICommand CallCommand { get; set; }
        public ICommand SendMessageCommand { get; set; }

        private string _contactId;
        public string ContactId
        {
            get => _contactId;
            set
            {
                _contactId = value;
                OnContactIdChanged();
            }
        }

        private string _name;
        public string Name
        {
            get => _name;
            set
            {
                SetProperty(ref _name, value);
            }
        }

        private string _surname;
        public string Surname
        {
            get => _surname;
            set
            {
                SetProperty(ref _surname, value);
            }
        }

        private List<PhoneNumber> _phoneNumbers;
        public List<PhoneNumber> PhoneNumbers
        {
            get => _phoneNumbers;
            set
            {
                _phoneNumbers = value;
                OnPropertyChanged();
            }
        }

        Contact Contact;

        public ContactDetailsViewModel(IContactsService _contactsService, INumbersService _numbersService, INavigationWrapper _navigationWrapper)
        {
            ContactsService = _contactsService;
            NumbersService = _numbersService;
            NavigationWrapper = _navigationWrapper;

            EditContactCommand = new RelayCommand((object arg) => ExecuteEditCommand());
            RemoveContactCommand = new RelayCommand((object arg) => ExecuteRemoveCommand());
            CallCommand = new RelayCommand((object arg) => ExecuteCallCommand());
            SendMessageCommand = new RelayCommand((object arg) => ExecuteSendMessageCommand());
        }

        private async Task OnContactIdChanged()
        {
            Contact = await ContactsService.GetContact(int.Parse(ContactId));

            Name = Contact.Name;
            Surname = Contact.Surname;
            PhoneNumbers = Contact.PhoneNumbers;
        }

        public void ExecuteEditCommand()
        {
            string uri = $"editcontact?index={Contact.Id}";
            NavigationWrapper.GoTo(uri).ConfigureAwait(false);
        }

        public void ExecuteRemoveCommand()
        {
            foreach(var number in PhoneNumbers)
            {
                NumbersService.DeletePhoneNumber(number);
            }

            ContactsService.DeleteContact(Contact);

            NavigationWrapper.GoToRoot().ConfigureAwait(false);
        }

        public void ExecuteCallCommand()
        {
            MessagingCenter.Send<ContactDetailsViewModel, string>(this, "CallButton", "You Clicked Call Button");
        }

        public void ExecuteSendMessageCommand()
        {
            MessagingCenter.Send<ContactDetailsViewModel, string>(this, "SendMessage", "You Clicked Send Message Button");
        }
    }
}
