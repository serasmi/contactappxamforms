﻿using ContactApp.Models.Entities;
using ContactApp.Services;
using ContactApp.Services.Wrappers.Navigation;
using ContactApp.ViewModels.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ContactApp.ViewModels
{
    public class EditContactViewModel : Base.BaseViewModel
    {
        public readonly IContactsService ContactService;
        public readonly INumbersService NumbersService;
        public readonly INavigationWrapper NavigationWrapper;

        private string _contactId;
        public string ContactId
        {
            get => _contactId;
            set
            {
                _contactId = value;
                OnContactIdChanged().ConfigureAwait(false);
            }
        }

        public Contact Contact = new Contact();

        public string Name
        {
            get => Contact.Name;
            set
            {
                Contact.Name = value;
                OnPropertyChanged();
            }
        }

        public string Surname
        {
            get => Contact.Surname;
            set
            {
                Contact.Surname = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<PhoneNumber> _phoneNumbers = new ObservableCollection<PhoneNumber>();
        public ObservableCollection<PhoneNumber> PhoneNumbers
        {
            get => _phoneNumbers;
            set
            {
                SetProperty(ref _phoneNumbers, value);
            }
        }

        public List<string> PhoneTypes
        {
            get
            {
                var t = Enum.GetNames(typeof(PhoneNumberType)).ToList();

                return t;
            }
        }

        public ICommand SaveCommand { get; set; }
        public ICommand AddPhoneNoCommand { get; set; }
        public ICommand RemovePhoneNoCommand { get; set; }

        public EditContactViewModel(IContactsService _contactsService, INumbersService _numbersService, INavigationWrapper _navigationWrapper)
        {
            ContactService = _contactsService;
            NumbersService = _numbersService;
            NavigationWrapper = _navigationWrapper;

            SaveCommand = new RelayCommand(async (object arg) => await SaveContact());
            RemovePhoneNoCommand = new RelayCommand((object arg) => RemovePhoneNumber(arg));
            AddPhoneNoCommand = new RelayCommand((object arg) => AddPhoneNumber());
        }

        public async Task OnContactIdChanged()
        {
            var contact = ContactService.GetContact(int.Parse(ContactId)).ConfigureAwait(true).GetAwaiter().GetResult();

            Contact.Id = contact.Id;
            Contact.PhoneNumbers = contact.PhoneNumbers;
            Name = contact.Name;
            Surname = contact.Surname;

            if(Contact.PhoneNumbers.Count > 0)
            {
                foreach (var phone in Contact.PhoneNumbers)
                {
                    PhoneNumbers.Add(phone);
                }
            }
        }

        public void RemovePhoneNumber(object arg)
        {
            PhoneNumber number = (PhoneNumber)arg;

            if (PhoneNumbers.Contains(number))
            {
                if(number.Id != 0)
                    NumbersService.DeletePhoneNumber(number);
                PhoneNumbers.Remove(number);
            }
        }

        public void AddPhoneNumber()
        {
            PhoneNumbers.Add(new PhoneNumber { NewNumber = true, IsCorrect = false });
            OnCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedAction.Add, "PhoneNumbers");
        }

        public async Task SaveContact()
        {
            Contact.PhoneNumbers = PhoneNumbers.Where(s => s.IsCorrect == true).ToList();
            try
            {
                if (!string.IsNullOrEmpty(Contact.Name))
                {
                    var newNumbers = Contact.PhoneNumbers.Where(s => s.IsCorrect == true).Where(s => s.NewNumber == true).ToList();
                    foreach (var number in newNumbers)
                    {
                        await NumbersService.AddPhoneNumber(number);
                    }
                    await ContactService.SaveContact(Contact);
                }
            }
            catch(Exception e)
            {
                throw e;
            }

            await NavigationWrapper.GoToRoot();
        }
    }
}
