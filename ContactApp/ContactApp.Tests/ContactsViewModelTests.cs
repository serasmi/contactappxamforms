﻿using ContactApp.Models.Entities;
using ContactApp.Services;
using ContactApp.ViewModels;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using System.Linq;
using ContactApp.Services.Wrappers.Navigation;

namespace ContactApp.Tests
{
    public class ContactsViewModelTests
    {
        [Fact]
        public void Constructor_Applies_Parameter_To_Class_Property()
        {
            var contactsServiceMock = new Mock<IContactsService>();
            var navigationWrapperMock = new Mock<INavigationWrapper>();

            var target = new ContactsViewModel(contactsServiceMock.Object, navigationWrapperMock.Object);

            Assert.Equal(contactsServiceMock.Object, target.ContactsService);
            Assert.Equal(navigationWrapperMock.Object, target.NavigationWrapper);
        }

        [Fact]
        public async Task LoadData_Is_Clearing_And_Adding_Items_To_Observable_Collection()
        {
            var returnedContacts = new List<Contact>
            {
                new Contact
                {
                    Id = 1
                },
                new Contact(),
                new Contact()
            };

            var contactsServiceMock = new Mock<IContactsService>();
            contactsServiceMock.Setup(s => s.GetContacts())
                .Returns(Task.FromResult(returnedContacts));
            var navigationWrapperMock = new Mock<INavigationWrapper>();

            var target = new ContactsViewModel(contactsServiceMock.Object, navigationWrapperMock.Object);

            foreach(Contact contact in returnedContacts)
            {
                target.Contacts.Add(contact);
            }

            await target.LoadData();

            Assert.True(target.Contacts.Count == returnedContacts.Count);
            Assert.True(string.Equals(target.Contacts.First().Id, returnedContacts.First().Id));
        }
    }
}
