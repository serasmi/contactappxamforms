﻿using ContactApp.Models.Entities;
using ContactApp.Services;
using ContactApp.ViewModels.Base;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ContactApp.Tests
{
    public class LoadMoreViewModelTests
    {
        //[Fact]
        //public async Task Constructor_Applies_Parameters_To_Class_Properties()
        //{
        //    var contactsServiceMock = new Mock<IContactsService>();

        //    var target = new LoadDataViewModel(
        //        contactsServiceMock.Object.GetContacts);

        //    Assert.Equal(contactsServiceMock.Object.GetContacts, target.LoadItems);
        //}

        //#region GetItems
        //[Fact]
        //public async Task GetItems_Parameter_Change_Page_Number()
        //{
        //    var returnedContacts = new List<Contact>
        //        {
        //            new Contact
        //            {
        //                Id = Guid.NewGuid().ToString()
        //            },
        //            new Contact(),
        //            new Contact()
        //        };

        //    var contactsServiceMock = new Mock<IContactsService>();
        //    contactsServiceMock.Setup(s => s.GetContacts(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()))
        //        .Returns(Task.FromResult(returnedContacts));

        //    var target = new LoadDataViewModel(
        //        contactsServiceMock.Object.GetContacts)
        //    {
        //        Page = 3
        //    };

        //    var contacts = await target.GetItems(true);

        //    Assert.True(target.Page == 1);
        //    Assert.True(target.IsFinalPage);
        //}

        //[Fact]
        //public async Task GetItems_Returns_Items_List()
        //{
        //    var returnedContacts = new List<Contact>
        //        {
        //            new Contact
        //            {
        //                Id = Guid.NewGuid().ToString()
        //            },
        //            new Contact(),
        //            new Contact()
        //        };

        //    var contactsServiceMock = new Mock<IContactsService>();
        //    contactsServiceMock.Setup(s => s.GetContacts(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()))
        //        .Returns(Task.FromResult(returnedContacts));

        //    var target = new LoadDataViewModel(
        //        contactsServiceMock.Object.GetContacts);

        //    var contacts = await target.GetItems(false);

        //    Assert.True(contacts.Count == returnedContacts.Count);
        //    Assert.True(string.Equals(contacts[0].Id, returnedContacts[0].Id));
        //}

        //[Fact]
        //public async Task GetItems_Is_Using_Service()
        //{
        //    var returnedContacts = new List<Contact>
        //        {
        //            new Contact
        //            {
        //                Id = Guid.NewGuid().ToString()
        //            },
        //            new Contact(),
        //            new Contact()
        //        };

        //    var contactsServiceMock = new Mock<IContactsService>();
        //    contactsServiceMock.Setup(s => s.GetContacts(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()))
        //        .Returns(Task.FromResult(returnedContacts))
        //        .Verifiable();

        //    var target = new LoadDataViewModel(
        //        contactsServiceMock.Object.GetContacts);

        //    var contacts = await target.GetItems(false);

        //    contactsServiceMock.Verify(s => s.GetContacts(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()), Times.Once);
        //}

        //[Fact]
        //public async Task GetItems_Service_Throws_An_Exception()
        //{
        //    var returnedContacts = new List<Contact>
        //        {
        //            new Contact
        //            {
        //                Id = Guid.NewGuid().ToString()
        //            },
        //            new Contact(),
        //            new Contact()
        //        };

        //    var contactsServiceMock = new Mock<IContactsService>();
        //    contactsServiceMock.Setup(s => s.GetContacts(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()))
        //        .Throws(new Exception());

        //    var target = new LoadDataViewModel(
        //        contactsServiceMock.Object.GetContacts);

        //    await Assert.ThrowsAsync<Exception>(() => target.GetItems(false));
        //}
        //#endregion
    }
}
