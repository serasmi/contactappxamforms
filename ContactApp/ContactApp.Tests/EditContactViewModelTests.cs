﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ContactApp.Models.Entities;
using ContactApp.Services;
using ContactApp.Services.Wrappers.Navigation;
using ContactApp.ViewModels;
using Moq;
using Xunit;

namespace ContactApp.Tests
{
    public class EditContactViewModelTests
    {
        [Fact]
        public void Constructor_Apply_Parameter_To_Class_Property()
        {
            var contactServiceMock = new Mock<IContactsService>();
            var phoneNoServiceMock = new Mock<INumbersService>();
            var navigationMock = new Mock<INavigationWrapper>();

            var target = new EditContactViewModel(contactServiceMock.Object, phoneNoServiceMock.Object, navigationMock.Object);

            Assert.Equal(contactServiceMock.Object, target.ContactService);
        }

        [Fact]
        public void Check_If_SaveContact_Is_Using_Service_And_Updating_Contact()
        {
            var contactServiceMock = new Mock<IContactsService>();
            contactServiceMock.Setup(s => s.SaveContact(It.IsAny<Contact>()))
                .Verifiable();
            var phoneNoServiceMock = new Mock<INumbersService>();
            var navigationMock = new Mock<INavigationWrapper>();

            var target = new EditContactViewModel(contactServiceMock.Object, phoneNoServiceMock.Object, navigationMock.Object);

            target.Contact.Id = 1;

            target.SaveCommand.Execute(null);

            contactServiceMock.Verify(s => s.SaveContact(It.IsAny<Contact>()));
        }

        [Fact]
        public void Check_If_SaveContact_Is_Using_Service_And_Adding_Contact()
        {
            var contactServiceMock = new Mock<IContactsService>();
            contactServiceMock.Setup(s => s.SaveContact(It.IsAny<Contact>()))
                .Verifiable();
            var phoneNoServiceMock = new Mock<INumbersService>();
            var navigationMock = new Mock<INavigationWrapper>();

            var target = new EditContactViewModel(contactServiceMock.Object, phoneNoServiceMock.Object, navigationMock.Object);

            target.SaveCommand.Execute(null);

            contactServiceMock.Verify(s => s.SaveContact(It.IsAny<Contact>()));
        }

        [Fact]
        public void Removing_Phone_Number_Where_There_Is_No_Phone_Number()
        {
            var contactServiceMock = new Mock<IContactsService>();
            contactServiceMock.Setup(s => s.DeleteContact(It.IsAny<Contact>()))
                .Verifiable();
            var phoneNoServiceMock = new Mock<INumbersService>();
            var navigationMock = new Mock<INavigationWrapper>();

            var target = new EditContactViewModel(contactServiceMock.Object, phoneNoServiceMock.Object, navigationMock.Object);

            int addedItemsNumber = 3;

            for (int i = 0; i < addedItemsNumber; i++)
            {
                target.PhoneNumbers.Add(new PhoneNumber
                {
                    Id = 1
                });
            }

            var searchedContact = new PhoneNumber
            {
                Id = 1
            };

            target.RemovePhoneNoCommand.Execute(searchedContact);

            Assert.True(target.PhoneNumbers.Count == addedItemsNumber);
            contactServiceMock.Verify(s => s.DeleteContact(It.IsAny<Contact>()), Times.Never);
        }

        [Fact]
        public void SaveContact_Updating_Contact_Use_Services()
        {
            var contactServiceMock = new Mock<IContactsService>();
            contactServiceMock.Setup(s => s.SaveContact(It.IsAny<Contact>()))
                .Returns(Task.FromResult(1))
                .Verifiable();
            var phoneNoServiceMock = new Mock<INumbersService>();

            var contact = new Contact
            {
                Id = 1
            };

            var listOfContact = new List<PhoneNumber>
            {
                new PhoneNumber()
                {
                    ContactId = contact.Id,
                    Contact = contact,
                    Number = "firstExistingNumber"
                },
                new PhoneNumber
                {
                    ContactId = contact.Id,
                    Contact = contact,
                    Number = "secondExistingNumber"
                },
                new PhoneNumber
                {
                    Number = "newTestNumber"
                }
            };

            var navigationMock = new Mock<INavigationWrapper>();

            var target = new EditContactViewModel(contactServiceMock.Object, phoneNoServiceMock.Object, navigationMock.Object)
            {
                Contact = contact
            };

            foreach (var number in listOfContact)
            {
                target.PhoneNumbers.Add(number);
            }

            target.SaveCommand.Execute(null);

            contactServiceMock.Verify(s => s.SaveContact(It.IsAny<Contact>()), Times.Once);
        }

        [Fact]
        public void SaveContact_Adding_New_Contact()
        {
            var contactServiceMock = new Mock<IContactsService>();
            contactServiceMock.Setup(s => s.SaveContact(It.IsAny<Contact>()))
                .Returns(Task.FromResult(1))
                .Verifiable();
            var phoneNoServiceMock = new Mock<INumbersService>();

            var contact = new Contact();

            var listOfContact = new List<PhoneNumber>
            {
                new PhoneNumber()
                {
                    Number = "firstExistingNumber"
                },
                new PhoneNumber
                {
                    Number = "secondExistingNumber"
                },
                new PhoneNumber
                {
                    Number = "newTestNumber"
                }
            };

            var navigationMock = new Mock<INavigationWrapper>();

            var target = new EditContactViewModel(contactServiceMock.Object, phoneNoServiceMock.Object, navigationMock.Object)
            {
                Contact = contact
            };

            foreach (var number in listOfContact)
            {
                target.PhoneNumbers.Add(number);
            }

            target.SaveCommand.Execute(null);

            contactServiceMock.Verify(s => s.SaveContact(It.IsAny<Contact>()), Times.Once);
        }

        [Fact]
        public void SaveContact_Add_Contact_Without_Numbers()
        {
            var contactServiceMock = new Mock<IContactsService>();
            contactServiceMock.Setup(s => s.SaveContact(It.IsAny<Contact>()))
                .Returns(Task.FromResult(1))
                .Verifiable();
            var phoneNoServiceMock = new Mock<INumbersService>();

            var contact = new Contact();

            var navigationMock = new Mock<INavigationWrapper>();

            var target = new EditContactViewModel(contactServiceMock.Object, phoneNoServiceMock.Object, navigationMock.Object)
            {
                Contact = contact
            };

            target.SaveCommand.Execute(null);

            contactServiceMock.Verify(s => s.SaveContact(It.IsAny<Contact>()), Times.Once);
        }
    }
}
